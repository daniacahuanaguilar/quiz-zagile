
from selenium.webdriver.common.by import By

class MainPageLocators(object):
    """A class for main page locators. All main page locators should come here"""
    USER_INPUT = (By.ID, 'username')
    CONTINUE_BUTTON = (By.ID, 'login-submit')
    PASSWORD_INPUT = (By.ID, 'password')
    LOGIN_BUTTON = (By.ID, 'login-submit')
    JIRA_BUTTON = (By.XPATH, '/html/body/div[2]/div[3]/div/div[2]/div[2]/div/div/div/div[3]/div/div[2]/a[7]/button/div')
    BUTTON_CREATE =(By.CLASS_NAME,'css-17vavsu')
    CREATE_SUMMARY = (By.ID,'summary')
    CREATE =(By.ID,'create-issue-submit')
   

class SearchResultsPageLocators(object):
    """A class for search results locators. All search results locators should come here"""
    pass


