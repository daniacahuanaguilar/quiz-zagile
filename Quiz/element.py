from selenium.webdriver.support.ui import WebDriverWait


class BasePageElement(object):
    """Base page class that is initialized on every page object class."""

    def __set__(self, obj, value):
       
        driver = obj.driver
        WebDriverWait(driver, 100).until(
            lambda driver: driver.find_element_by_id(self.locator))
        driver.find_element_by_id(self.locator).clear()
        driver.find_element_by_id(self.locator).send_keys(value)

    def __get__(self, obj, owner):
      
        driver = obj.driver
        WebDriverWait(driver, 100).until(
            lambda driver: driver.find_element_by_id(self.locator))
        element = driver.find_element_by_id(self.locator)
        return element.get_attribute("value")

class BasePageElement2(object):
    """Base page class that is initialized on every page object class."""

    def __set__(self, obj, value):
       
        driver = obj.driver
        WebDriverWait(driver, 100).until(
            lambda driver: driver.find_elements_by_class_name(self.locator))
       

    def __get__(self, obj, owner):
      
        driver = obj.driver
        WebDriverWait(driver, 100).until(
            lambda driver: driver.find_elements_by_class_name(self.locator))
        element = driver.find_elements_by_class_name(self.locator)
        return element.get_attribute("value")










