from element import BasePageElement
from element import BasePageElement2


from locators import MainPageLocators
import time
from selenium import webdriver

class SearchTextElement(BasePageElement):
   
    locator = 'username'

class SearchTextElement2(BasePageElement):
    
    locator = 'password'

class SearchTextElement3(BasePageElement2):
  
    locator = 'css-n39e01'


class SearchTextElement4(BasePageElement):
    
    locator = 'summary'

class SearchTextElement5(BasePageElement):
    
    locator = 'description'



class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""

    def __init__(self, driver):
        self.driver = driver


class MainPage(BasePage):
    """Home page action methods come here. I.e. """

    #Declares a variable that will contain the retrieved text
    search_text_element = SearchTextElement()
    search_text_element2 = SearchTextElement2()
    search_text_element3 = SearchTextElement3()   
    search_text_element4 = SearchTextElement4()
    search_text_element5 = SearchTextElement5()
    
    
  

    def is_title_matches(self):
        """Verifies that the hardcoded text "Atlassian" appears in page title"""
        return "Atlassian" in self.driver.title


    def user_input(self):
        """Triggers the search"""
        element = self.driver.find_element(*MainPageLocators.USER_INPUT)

    def click_continue_button(self):
        """Triggers the search"""
        element = self.driver.find_element(*MainPageLocators.CONTINUE_BUTTON)
        element.click()

    def click_login_button(self):
        """Triggers the search"""
        element = self.driver.find_element(*MainPageLocators.LOGIN_BUTTON)
        element.click()

    def click_button_jira(self):
        """Triggers the search"""
        element = self.driver.find_element(*MainPageLocators.JIRA_BUTTON)
        element.click()

    def click_button_create(self):
        """Triggers the search"""       
	element = self.driver.find_element(*MainPageLocators.BUTTON_CREATE)
        element.click()
	
    def prompt_alert(self):
    	time.sleep(2)
	
    def button_create(self):
        """Triggers the search"""
        element = self.driver.find_element(*MainPageLocators.CREATE_SUMMARY)
        
    def create_issue(self):
        """Triggers the search"""
        element = self.driver.find_element(*MainPageLocators.CREATE)


class SearchResultsPage(BasePage):
    """Search results page action methods come here"""

    def is_results_found(self):
        # Probably should search for this text in the specific page
        # element, but as for now it works fine
        return "No results found." not in self.driver.page_source

