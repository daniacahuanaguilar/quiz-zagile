import unittest
from selenium import webdriver
import page


class AtlassianLogin(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.get("https://id.atlassian.com/login?")

    def test_login(self):
  
        #Load the main page. In this case the home page.
        main_page = page.MainPage(self.driver)

        #Checks if the word "Atlassian" is in title
        assert main_page.is_title_matches()

	main_page.user_input()

        main_page.search_text_element = "daniacahuanaguilar@gmail.com"

	main_page.click_continue_button()

        main_page.search_text_element2 = "qwertyuiop"

        main_page.click_login_button()

	main_page.search_text_element3 = "d"

	main_page.prompt_alert()

	main_page.click_button_jira()

	self.driver.refresh()

	main_page.click_button_create()
	
	main_page.prompt_alert()
	
	main_page.button_create()

	main_page.search_text_element4 = "This is a summary of test"

	main_page.search_text_element5 = "This is a description of test"

	main_page.create_issue()

	main_page.prompt_alert()
	

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()

